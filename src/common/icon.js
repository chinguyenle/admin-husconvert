import React from 'react';
import classnames from 'classnames'
import { IconArrays } from './icon-name';
import PropsType from 'prop-types'



const getIcon =(name)=>{
    let icon = IconArrays.filter(item=>item.name===name)
    return icon[0].src;
}

export const Icon = ({name, className, onClick}) => (
    <img src={getIcon(name)} className={classnames("icon", className)} onClick={onClick} />
);

Icon.propTypes = {
    name: PropsType.string,
    className: PropsType.string,
    onClick: PropsType.func
}