import { ACCESS_TOKEN } from "../constants";

const lcStorage = {
  setAccessToken:(token) => localStorage.setItem(ACCESS_TOKEN, token),
  removeAccessToken: () => localStorage.removeItem(ACCESS_TOKEN),
  getAccessToken: () => localStorage.getItem(ACCESS_TOKEN)
}


export {lcStorage};