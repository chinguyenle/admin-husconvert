import React from 'react';

import LoveLineIcon from '../assets/icon/icon-love-line.png'
import CommentIcon from '../assets/icon/icon-comment.svg'
import PlayIcon from '../assets/icon/icon-play.svg'
import PauseIcon from '../assets/icon/icon-pause.svg'
import ReplayIcon from '../assets/icon/icon-replay.svg'
import VolumeUpIcon from '../assets/icon/icon-volume-up.svg'
import VolumeDownIcon from '../assets/icon/icon-volume-down.svg'
import VolumeOffIcon from '../assets/icon/icon-volume-off.svg'
import CommentShowIcon from '../assets/icon/icon-comment-show.png'
import CommentHiddenIcon from '../assets/icon/icon-comment-hidden.png'


export const IconName = {
  LoveLine: 'love line',
  LoveFull: 'love full',
  Comment: 'comment',
  Play: 'play',
  Pause: 'pause',
  Replay: 'replay',
  VolumeUp: 'volume up',
  VolumeDown: 'volume down',
  VolumeOff: 'volume off',
  CommentShow: 'Comment show',
  CommentHidden: 'Comment hidden'

}

export const IconArrays = [
  {
    name: IconName.LoveLine,
    src: LoveLineIcon
  },
  {
    name: IconName.Comment,
    src: CommentIcon
  },
  {
    name: IconName.Play,
    src: PlayIcon
  },
  {
    name: IconName.Pause,
    src: PauseIcon
  },
  {
    name: IconName.Replay,
    src: ReplayIcon
  },
  {
    name: IconName.VolumeUp,
    src: VolumeUpIcon,
  },
  {
    name: IconName.VolumeDown,
    src: VolumeDownIcon,
  },
  {
    name: IconName.VolumeOff,
    src: VolumeOffIcon
  },
  {
    name: IconName.CommentShow,
    src: CommentShowIcon
  },
  {
    name: IconName.CommentHidden,
    src: CommentHiddenIcon
  }


]