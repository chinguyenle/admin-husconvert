import React, { Component } from 'react';

import { Provider } from 'react-redux';
import _ from 'lodash'
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './redux/configureStore'

import { browserHistory } from './common/history';
import './App.scss'
import AppLayout from './routes/author/app-layout';
import Login from './routes/guest-routes/login';

class App extends Component {
  render() {
    return (
      <div className="customer">
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router history={browserHistory}>
            <div className="container">
              <div className="content">
                <Switch>
                  <Route exact path="/" render={() => (<Redirect to="/login" />)} />
                  <Route path="/login" render={() => {
                    console.log('store', store, _.isEmpty(store.getState().user) );
                    if(_.isEmpty(store.getState().user))
                     return(<Login/>)
                    return(<Redirect to="/home" />)
                     }} />
                  <AppLayout />

                </Switch>

              </div>
            </div>
          </Router>
        </PersistGate>
      </Provider>
      </div>
    );
  }
}

export default App;
