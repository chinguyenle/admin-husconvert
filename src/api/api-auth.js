import { api } from './api'

export const apiAuth = {
  login: ({usernameOrEmail, password}) => api.post('auth/signin', { usernameOrEmail, password }),
  
  signup: ({name, email, username, password, dateOfBirth, avatar}) => 
      api.postFormData('auth/signup', { name, email, username, password, dateOfBirth, avatar}),
  createAvatar: ({avatar}) => api.postImage('auth/up', {avatar})
}


