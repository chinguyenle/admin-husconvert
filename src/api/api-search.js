import { api } from "./api";

export const apiSearch = {
  search: (keyWord) => api.get('search', keyWord)
}