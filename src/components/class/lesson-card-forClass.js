import React, { Component } from 'react';
import {connect} from 'react-redux'

import classnames from 'classnames'
import { Avatar } from '../avatar/avatar';
import moment from 'moment';
import { Love, Comment } from '../slide/reaction/reaction';

import './lesson-card.scss'
import { getUrlFile } from '../../common/funtions';
import { apiUser } from '../../api/api-user';
import { Spin } from 'antd';

class LessonCardForClass extends Component {
    constructor(props) {
        super(props);
        this.state={
          createdBy: null
        }
    }
    componentDidMount(){
      const { lessonInfo} = this.props;
      const {createdBy } = lessonInfo;
      this.getUser(createdBy);
    }

    setActiveLike(){
        const { lessonInfo } = this.props;
        const {user}= this.props;
        let activeLike = false;
        if(lessonInfo.users.length >0){
            lessonInfo.users.map(item => {
               if(item.id === user.id) activeLike = true;
            })
        }
        return activeLike;
    }
    getUser(userId){
      apiUser.getProfilerById(userId)
      .then(res =>{
        // console.log('res', res);
        if(res.id)
          this.setState({
            createdBy: res.user
          })
      })
      .catch(e => console.log(e))
    }

    render() {
        const { lessonInfo, onClick, className } = this.props;
        const { name, avatarId, type, users, comments, auth, createdAt } = lessonInfo;
        const {createdBy} = this.state;
        let activeLike = this.setActiveLike();
        return (
            <div  className={classnames("lesson-card",className)} onClick={onClick}>
                <div 
                className="lesson-card-header" 
                style={{
                    backgroundImage:`url(${getUrlFile(avatarId)})`, 
                    backgroundRepeat:'no-repeat', 
                    backgroundSize:'cover', 
                    backgroundPosition:'center', 
                        // filter:' blur(0.8px)'
                    }}>
                    {/* <img className="lesson-card-image" alt="card" src={lessonAvatar} /> */}
                    <div className="blur-background type">{type?type:"Khoa học, Giáo dục"}</div>
                    <div className="blur-background lesson-name">{name}</div>
                </div>
                <div className="interactions">
                    <Love active={activeLike} number={users.length} onClick={(love) => console.log('love', love)} />
                    <Comment number={comments}/>
                </div>
                <div className="lesson-card-footer">
                   {createdBy? <Avatar
                        avatar={getUrlFile(createdBy.avatarId)}
                        name={createdBy.name}
                        hasName
                    />:<Spin/>}
                    <div className="create-at">{moment(createdAt).format('DD/MM/YYYY')}</div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user
  });
  
export default connect(mapStateToProps)(LessonCardForClass)