import React, { Component } from 'react'
import LoveLine from '../../../assets/icon/icon-love-line.png'
import LoveFull from '../../../assets/icon/icon-love-full.png'
import CommentIcon from '../../../assets/icon/icon-comment.svg'

import './reaction.scss'


class Love extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: props.active,
      number: props.number
    }
  }
  handleClick() {
    const { onClick } = this.props;
    const { active, number } = this.state;
    this.setState({
      active: !active,
      number: !active ? (number + 1) : number>0? (number - 1):0
    }, () => onClick(!active))
  }
  render() {
    const { active, number } = this.state;
    return (
      <div className="reaction" onClick={() => this.handleClick()}>
        <img src={active ? LoveFull : LoveLine} className="icon" />
        <div className="number">{number}</div>
      </div>
    );
  }
}

class Comment extends Component {
  render() {
    const { number, href } = this.props;
    return (
      <div className="reaction" href={href}>
        <img src={CommentIcon} className="icon" />
        <div className="number">{number}</div>
      </div>
    );
  }
}

export { Love, Comment }