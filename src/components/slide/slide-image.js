import React,  {Component} from 'react'


import './slide-image.scss'


export class SlideImage extends Component {
  render(){
    const {src, className, name, onClick } = this.props;
    return(
      <div className={"slide-image " + className} onClick={onClick}>
        <img src={src} className="image" />
        <div className="name">{name}</div>
      </div>
    );
  }
}