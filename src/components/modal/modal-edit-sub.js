import React, { Component } from 'react'
import { Select } from 'antd'

import './modal-edit-sub.scss'
import { Modal } from './modal';
import ButtonBorder from '../button/button-border';

const Option = Select.Option;

const TEXTSETTING = [
  {
    title: "font",
    options: ["aril", "Time new roman", "aboto"]
  },
  {
    title: "font size",
    options: [14, 15, 58]
  },
  {
    title: "color",
    options: ["#fe4a49", " #2ab7ca", "#fed766", " #e6e6ea", "#011f4b", "#851e3e"]
  }
]
const SOUNDSETTING = [
  {
    title: "voice", options: [
      { label: "Mạnh Dũng (Nam Hà Nội)", value: "hn_male_xuantin_vdts_48k-hsmm" },
      { label: "Thùy Linh (Nữ Hà Nội)", value: "hn_female_thutrang_phrase_48k-hsmm" },
      { label: "Mai Phương (Nữ Hà Nội)", value: "hn_female_xuanthu_news_48k-hsmm" },
      { label: "Nhất Nam (Nam Sài Gòn)", value: "sg_male_xuankien_vdts_48k-hsmm" },
      { label: "Lan Trinh (Nữ Sài Gòn)", value: "sg_female_xuanhong_vdts_48k-hsmm" },
      { label: "Thảo Trinh (Nữ Sài Gòn)", value: "sg_female_thaotrinh_dialog_48k-hsmm" },
      { label: "Minh Hoàng (Nam Sài Gòn)", value: "sg_male_minhhoang_dial_48k-hsmm" },
    ]
  },
  {
    title: "speed", options: [
      { label: "0.5x", value: "0.5" },
      { label: "1x", value: "1.0" },
      { label: "1.25x", value: "1.25" },
      { label: "1.5x", value: "1.5" },
      { label: "1.9x", value: "1.9" },
    ]
  }
]


const FormatContainer = (props) => {
  const { title, children } = props;
  return (
    <div className="format-container">
      <div className="format-title">{title}</div>
      <div className="format-content">
        {children}
      </div>
    </div>
  )
}

class ModalEditSub extends Component {
  constructor(props) {
    super(props);
    console.log('props', this.props);
    this.state = {
      voiceType: props.subInfo.voiceType,
      readdingSpeed: props.subInfo.readdingSpeed,
    }
  }
  onChange(stateField, value) {
    this.setState({
      [stateField]: value
    })
  }
  setVoice(){
    const {onOk, subInfo, index} = this.props;
    let newSub = {...subInfo}
    newSub.voiceType = this.state.voiceType;
    newSub.readdingSpeed = this.state.readdingSpeed;
    console.log('key', index)
    onOk(index, newSub)
    Modal.dismiss();
  }
  render() {
    const {voiceType, readdingSpeed} = this.state
    return (
      <div className="edit-sub-container">
        <FormatContainer
          title="Sound"
        >
          {SOUNDSETTING.map(setText => (
            <div className="edit-sub-type">
              <div className="type-title">{setText.title}</div>
              <Select className="select-edit" defaultValue={setText.title === "voice"?voiceType:readdingSpeed} onChange={(value) => {
                if (setText.title === "voice")
                  this.onChange("voiceType", value)
                else this.onChange("readdingSpeed", value)
              }}>
                {
                  setText.options.map(option => (

                    <Option key={Math.random()} value={option.value}>{option.label}</Option>
                  ))}
              </Select>
            </div>
          ))}
        </FormatContainer>
        <div className="bottom-button">
          <ButtonBorder content="Cancel" onClick={()=> Modal.dismiss()}/>
          <ButtonBorder content="Ok" onClick={()=>this.setVoice()}/>
        </div>
      </div>
    );
  }
}

export const openSettingModal = (index, info, changeVoice) => {
  Modal.show({
    content: (
      <ModalEditSub
        index={index}
        subInfo={info}
        onOk={(index, voice)=>changeVoice(index, voice)}
      />
    )
  })

}