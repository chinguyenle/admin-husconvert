import React from 'react';
import {Modal as VendorModal} from 'antd';

import './modal.scss'

export class Modal {

  static show({content, onClose, onOk}) {
    VendorModal.info({
      content:(
        <div>
          {content}
        </div>
      ),
      onCancel:()=>{VendorModal.destroyAll()},
      className:"modal-container"
    })
  }
  static dismiss(){
    VendorModal.destroyAll();
  }
}