import React,  {Component} from 'react'
import classnames from 'classnames'

import './button-icon.scss'
import { Icon } from '../../common/icon';


export class ButtonIcon extends Component {
  render(){
    const {iconName, title, className, onClick} = this.props;
    return(
      <div className={classnames("button-icon", className)} onClick={onClick} >
          {title}
          <Icon name={iconName} className="icon"/>
      </div>
    );
  }
}

