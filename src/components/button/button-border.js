import React,  {Component} from 'react'
import classnames from 'classnames'

import './button-border.scss'


export default class ButtonBorder extends Component {
  render(){
    const {content, onClick, className, ...otherProps} = this.props;
    return(
      <button className={classnames("button-border", className)} onClick={onClick} {...otherProps}>
          <div className="button-content">{content}</div>
      </button>
    );
  }
}