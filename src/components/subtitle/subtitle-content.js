import React,  {Component} from 'react'
import {Icon} from 'antd'
import SpeakerIcon from '../../assets/icon/icon-speaker.png'
import ThreeIcon from '../../assets/icon/three-dots.png'
import './subtitle-content.scss'
import TextArea from 'antd/lib/input/TextArea';


export default class SubtitleContent extends Component {

  constructor(props){
    super(props);
    this.state={
      subtitle:{...props.subtitle}
    }
  }
  componentWillReceiveProps(nextProps){
    // console.log('next poprs', nextProps);
    if(this.props!= nextProps){
      this.setState({
        subtitle: {...nextProps.subtitle}
      })
    }
  }
  
  calculateTimeToTalk(text: String) {
    if(text.trim().length > 0) {
      console.log(" time", Math.ceil(text.split(" ").length * 60 / 250 ))
     return Math.ceil(text.split(" ").length * 60 / 250 )
    }
    return 0
  }
  changeSubtitle(key, value:String){
    let {subtitle} = this.state;
    const {onChangeSub, index} = this.props
    subtitle[key]= value;

    if((subtitle.startTime + this.calculateTimeToTalk(value.trim())) > subtitle.endTime) 
      subtitle.endTime = subtitle.startTime + this.calculateTimeToTalk(value)
    
    this.setState({
      subtitle: subtitle
    }, ()=>onChangeSub(index, subtitle))
  }
  render(){
    const {subtitle} = this.state;
    const {openSetting, deleteSub} = this.props
    // console.log('sub---', subtitle);
    // console.log('sub-props-', subtitle);
    return(
      <div className="subtitle-content">
        <Icon type="close" className="delete-sub" onClick={deleteSub} />
        <TextArea className="input" value={subtitle.text} onChange={e=>this.changeSubtitle('text', e.target.value)} />
        <div className="time">{subtitle.startTime}-{subtitle.endTime}</div>
        <div className="speaker icon" onClick={()=> console.log('speaker')}>
          <img src={SpeakerIcon} alt="speaker" />
        </div>
        <div className="icon" onClick={()=> openSetting()}>
          <img src={ThreeIcon} alt="three dots" />
        </div>
      </div>
    );
  }
}