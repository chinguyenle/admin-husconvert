export const INTRODUCE =[
  {title: "Create lessons easily",
    description:"Upload your file simply by dropping them into the box or selecting the \" Choose file\" option. Add subtitle if you want. And our servers will create video and you just have to download it."
  },
  {
    title: "Adding a voice",
    description:"A voice will be added automatically based on the subtitles, which you add for each slide. You can choose the type of voice or reading speed."
  },
  {
    title: "Mission",
    description: "Giving you the most convenient and quality products"
  },
  {
    title:"About us",
    description:"Director: Nguyen Van A \r\n Design: Nguyen Van A \r\n Dev team: Nguyen Van A",
  },
]