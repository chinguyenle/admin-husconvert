import React, { Component } from 'react'
import iconPPT from '../../assets/icon/icon-ppt.png'

import Dropzone from 'react-dropzone'
import './choose-file.scss'


export class ChooseFile extends Component {


  handleSelectedFile = (file) => {
    const {handleSelectedFile} = this.props;
    console.log(file);
    if(file)
      handleSelectedFile(file);
  }
  render() {
    return (
      <div className="box">
        <Dropzone onDrop={file => this.handleSelectedFile(file)}>
          {({ getRootProps, getInputProps }) => (
            <div className="box-overlay"  {...getRootProps()}>
              <input {...getInputProps()} />
              <img className="icon-ppt" src={iconPPT} alt="img" />
              <div className="text drop " >Drop PPT File here</div>
              <div className="upload-btn-wrapper">
                <div className="text choose">Choose file</div>
                <input type="file" name="myFile" onChange={(e)=> this.handleSelectedFile(e.target.files[0])} onClick={e => e.stopPropagation()} />
              </div>
            </div>
          )}
        </Dropzone>
      </div>
    );
  }
}