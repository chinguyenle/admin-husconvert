import React, { Component } from 'react'
import { Dropdown, Menu } from 'antd'

import './drop-down.scss'
import { browserHistory } from '../../common/history';
import { logout } from '../../common/funtions';


export class DropDownCus extends Component {
  render() {
    const { menu, children, logout, myLession, account, onClickAccount } = this.props;
    return (
      <Dropdown className="drop-down" overlay={menu ? menu : exampleMenu({ logout, myLession, account, onClickAccount })} trigger={['click']} placement="bottomLeft">
        <div>
          {children}
        </div>
      </Dropdown>
    );
  }
}

const exampleMenu = ({ account, onClickAccount }) => (
  <Menu>
    <Menu.Item key={0} onClick={() => onClickAccount()}>
      Account
    </Menu.Item>
    <Menu.Item key={2} onClick={() => logout()}>
      Log out
    </Menu.Item>
  </Menu>
)

