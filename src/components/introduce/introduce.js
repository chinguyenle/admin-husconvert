import React, { Component } from 'react'


import './introduce.scss'


export default class Introduce extends Component {
  render() {
    const { title, description } = this.props;
    return (
      <div className="introduce">
        <div className="title">
          {title}
        </div>
        <div className="description">
          {description}
        </div>
        {/* <iframe src="https://onedrive.live.com/embed?cid=B8E6E1343A98940C&resid=B8E6E1343A98940C%21966&authkey=APjK0BUHTZvkiQo&em=2" width="402" height="327" frameborder="0" scrolling="no"></iframe> */}
      </div>
    );
  }
}