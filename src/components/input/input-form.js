import React,  {Component} from 'react'


import './input-form.scss'


export default class InputForm extends Component {
  render(){
    const {title, onChange, type="text"} = this.props;
    return(
      <div className="input-form">
        <div className="title">{title}</div>
        <input className="input" onChange={onChange} type={type}/>
      </div>
    );
  }
}