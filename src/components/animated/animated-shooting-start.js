import React,  {Component} from 'react'


import './animated-shooting-start.scss'


export default class AnimatedShootingStar extends Component {
  render(){
    return(
      <div className="animated-container">
        <div class="p p-1"></div>
        <div class="p p-2"></div>
        <div class="p p-3"></div>
      </div>
    );
  }
}