import React,  {Component} from 'react'
import moment from 'moment'

import './comment-user.scss'


export class CommentUser extends Component {
  render(){
    const {user, text, createdAt, key} = this.props.comment;
    return(
      <div className="comment-user">
        <div className="user-name"><span>{user.name}</span></div>
        <div className="content">{text}</div>
        <div className="time">{moment.utc(createdAt).local().format('HH:mm - L')}</div>
      </div>
    );
  }
}