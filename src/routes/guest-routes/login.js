import React, { Component } from 'react'
import './login.scss'

import { store } from '../../redux/configureStore'
import { Form, Input, Icon, Button, notification } from 'antd';
import { browserHistory } from '../../common/history';
import { apiAuth } from '../../api/api-auth';
import { lcStorage } from '../../common/util';
import { apiUser } from '../../api/api-user';
import { updateUser } from '../../redux/actions/user-actions';
import { ROLE } from '../../constants';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    console.log('props', props)
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  login = () => {
    notification.success({
      message: 'Polling App',
      description: "You're successfully logged in.",
    });
    browserHistory.push('/home');
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('email', values)
       apiAuth.login(values)
          .then( res => {
            console.log('respose', res);
            if (!res.error) {
              lcStorage.setAccessToken(res.accessToken)
              apiUser.getProfile()
              .then(async resProfile => {
                console.log('profile', resProfile);
                let isTeacher = resProfile.user.roles[0].name===ROLE.ROLE_TEACHER;
               await store.dispatch(updateUser({...resProfile, isTeacher: isTeacher}))
                this.login();
            })
              .catch(e => console.log(e))
            }
            else {
              if (res.status === 401) {
                notification.error({
                  message: 'HusConvert',
                  description: 'Your Username or Password is incorrect. Please try again!'
                });
              } else {
                notification.error({
                  message: 'HusConvert',
                  description: res.message || 'Sorry! Something went wrong. Please try again!'
                });
              }
            }
          })

          .catch(error => console.log(error));
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    console.log('show login')
    return (
      <div className="limiter">
        <div className="container-login100" style={{ backgroundColor: "#0faf00" }} >
          <div className="wrap-login100">
            <Form onSubmit={this.handleSubmit} className="login-form login100-form validate-form">

              <span className="login100-form-title">
                Log in
              </span>
              <Form.Item>
                {getFieldDecorator('usernameOrEmail', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input
                    className="input100"
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="Username or Email"
                    name="usernameOrEmail"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input
                    className="input100"
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    placeholder="Password"
                  />
                )}
              </Form.Item>
              <Form.Item>
                {/* {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: true,
                })} */}
                  <a className="login-form-forgot">
                    Forgot password
                    </a>
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    Log in
                   </Button>
                  {/* Or <a href="">register now!</a> */}
                
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
const Login = Form.create({ name: 'login' })(LoginForm);


export default Login