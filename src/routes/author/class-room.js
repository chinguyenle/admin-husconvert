import React, { Component } from 'react'


import './classes.scss'
import { apiClass } from '../../api/api-class';
import swal from 'sweetalert';
import { Table, Divider, Input, Button, Icon, Tag, InputNumber, Popconfirm, Form, DatePicker, Radio } from 'antd';
import Highlighter from 'react-highlight-words';
import moment from 'moment'
import { formateDate } from '../../util/util';
import { ROLE } from '../../constants';
import { ModalAddUser } from './modal/modal-add-user';

const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: inputType() === "email"
                ? [
                  {
                    required: true,
                    message: `Please Input ${title}!`,
                  },

                  { type: 'email', message: 'The input is not valid E-mail!' }
                ]
                : [

                  {
                    required: true,
                    message: `Please Input ${title}!`,
                  }
                ],
              initialValue: inputType() === 'date' ? moment(record[dataIndex]) : inputType() === 'roles' ? record.roles[0].name : record[dataIndex],
            })(this.getInput())}
          </Form.Item>
        ) : (
            children
          )}
      </td>
    );
  };

  render() {
    // console.log('o ptops', this.props)
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}

class ClassRoomTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classRoom: [],
      editingKey: '',
      searchText: '',
      users: null,
      showAddUserModal: false,
      selectedClass:null
    }
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        width: '5%',
        editable: false,
      },
      {
        title: 'Class Name',
        dataIndex: 'classRoomName',
        key: 'classRoomName',
        width: '20%',
        ...this.getColumnSearchProps('classRoomName'),
        editable: true,
      },
      {
        title: 'Created At',
        dataIndex: 'name',
        key: 'name',
        width: '20%',
        render: (text) => (<span>{moment(text).format("DD-MM-YYYY")}</span>),
        editable: false,
      },
      {
        title: 'Created By',
        dataIndex: 'creator',
        key: 'creator',
        width: '15%',
        ...this.getColumnSearchProps('creator'),
        editable: false,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        width: '15%',
        ...this.getColumnSearchProps('email'),
        editable: false,
      },
      {
        title: 'Lessons',
        dataIndex: 'lessons',
        render: (lessons) => (<span>{lessons.length}</span>),
        key: 'lessons',
        width: '10%',
        editable: false,
      },
      {
        title: 'Members',
        children: [
          {
            title: 'Numbers',
            dataIndex: 'users',
            render: (lessons) => (<span>{lessons.length}</span>),
            key: 'users',
            width: '5%',
          },
          {
            title: 'Actions member',
            dataIndex: 'operation-members',
            render: (lessons, record) => ((
              <span>
                <Button type="primary" onClick={() => this.addUserToClass(record)} style={{margin: 5}}>
                <Icon type="plus" />
                </Button> 
                <Button type="primary" onClick={() => this.removeUserFromClass(record)} style={{ color: 'red', margin: 5 }}>
                <Icon type="minus" />
                </Button>
              </span>
            )),
            width: '10%',
          },
        ]
      },
      {
        title: 'Action',
        dataIndex: 'operation',
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    href="javascript:;"
                    onClick={() => this.save(form, record.id)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.id)}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
              <span>
                <a disabled={editingKey !== ''} onClick={() => this.edit(record.id)} style={{ marginRight: 20 }}>
                  Edit
              </a>
                <a disabled={editingKey !== ''} onClick={() => this.delete(record.id)} style={{ color: 'red' }}>
                  Delete
              </a>
              </span>
            );
        },
      }
    ];
  }
  //
  isEditing = record => record.id === this.state.editingKey;
  cancel = () => {
    this.setState({ editingKey: '' });
  };

  save(form, key) {
    form.validateFields((error, row) => {
      console.log('error', error, row)
      if (error) {
        return;
      }

      const newData = [...this.state.classRoom];
      const index = newData.findIndex(item => key === item.id);
      console.log('index', key,index, row)
      if (index > -1) {
        swal({
          title: "Save your change ?",
          icon: "warning",
          buttons: true,
        })
          .then((value) => {
            if (value) {
              let params ={
              ...row,
              classId: key
            }
              apiClass.editClassRoom(params)
              .then(res => {
                if(res.success){
                  swal({
                    title: "Information is changed",
                    icon: "success",
                    buttons: true,
                  })
                  this.getAllClasses();
                  this.setState({ editingKey: '' });
                }
              })
            }
          });

      } else {
        newData.push(row);
        this.setState({ users: newData, editingKey: '' });
      }
    });
  }

  edit(key) {
    this.setState({ editingKey: key });
  }

  delete(key) {
    swal({
      title: "Delete this class",
      text: "when class is deleted, all class's infomation is delete too!",
      icon: "warning",
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          apiClass.deleteClass(key)
          .then(res => {
            if(res.success){
              swal({
                title: "Class is deleted!",
                icon: "success",
              }).then((value) => {
                console.log('value', value);
                this.getAllClasses();
                
              })
            }
            else{
              swal({
                title: "There are several errors that have occurred!",
                message: "please, try again later",
                icon: "error",
              }).then((value) => {
                console.log('value', value);
                
              })
            }
          })
          .catch(e => console.log(e))

        }
      });
  }
  addUserToClass(classInfo){
    console.log('calss infor', classInfo)
    this.setState({
      showAddUserModal: true,
      selectedClass:classInfo,
      addUser: true
    })
  }
  removeUserFromClass(classInfo){
    this.setState({
      showAddUserModal: true,
      selectedClass:classInfo,
      addUser: false
    })
  }
  //
  getAllClasses = () => {
    apiClass.getAllClasses()
      .then(res => {
        if (res.size > 0) {
          let tempClass = []
          res.content.map(item => {
            let { id, classRoomName, createdAt, lessons, users } = item.classRoom
            let { name, email } = item.createdBy;
            let classObject = {
              id, classRoomName, createdAt, lessons, users, creator: name, email
            }
            tempClass.push(classObject)
          })
          this.setState({
            classRoom: tempClass
          })
        }
      })
      .catch(e => console.log(e))
  }

  componentDidMount() {
    this.getAllClasses()
  }

  //// handle in table
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
      </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
      </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      text ? <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
        : null
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };
  handleTableChange = (pagination, filters, sorter) => {

  }
  ///
  render() {
    const { classRoom, showAddUserModal, selectedClass, addUser } = this.state;
    console.log('class state', this.state)
    const components = {
      body: {
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: () => {
            if (col.dataIndex === 'dateOfBirth') return 'date';
            if (col.dataIndex === 'email') return 'email';
            if (col.dataIndex === 'roles') return 'roles';
            return "text"
          },
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });
    return (
      <EditableContext.Provider value={this.props.form}>

        <Table
          components={components}
          rowKey={record => record.id}
          columns={columns}
          dataSource={classRoom}
          // onChange={this.handleTableChange}
          pagination={{
            onChange: this.cancel,
          }}
          rowClassName="editable-row"
          bordered
        />
        
      <ModalAddUser
        visible={showAddUserModal}
        addUser={addUser}
        classId={selectedClass?selectedClass.id:null}
        usersInClass={selectedClass?selectedClass.users:[]}
        onCancel={()=>{this.setState({showAddUserModal: false}); this.getAllClasses()}}
      />

      </EditableContext.Provider>
    );
  }
}
const ClassRoom = Form.create()(ClassRoomTable);

export default ClassRoom