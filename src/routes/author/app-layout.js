
import './app-layout.scss'
import Home from './home';
import User from './user';
import ClassRoom from './class-room';
import React, { Component } from 'react'
import classnames from 'classnames'

import { Route, Redirect, Link } from 'react-router-dom';
import { Layout, Menu, Icon, } from 'antd';
import _ from 'lodash'

import MyAccountModal from '../author/modal/modal-account'
import { DropDownCus } from '../../components/dropdown/drop-down'
import { Avatar } from '../../components/avatar/avatar'
import { connect } from 'react-redux';
import { browserHistory } from '../../common/history';
import { getUrlFile } from '../../common/funtions';

import convertIcon from '../../assets/images/convert.png'
import HusConvert from '../../assets/images/HusConvert.png'
const { Header, Content, Sider } = Layout;


class AppLayout extends Component {
  state = {
    collapsed: false,
    showMyAccountModal: false
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  handleSelectItem() {
    const { location } = this.props;
    return location.pathname === '/home' ? ['1'] : location.pathname === '/user' ? ['2'] : ['3']
  }

  render() {
    const { user } = this.props;
    const { showMyAccountModal } = this.state;
    console.log('user', this.props)
    // console.log('browser', browserHistory)
    return (
      <Layout className="layout-custom">
        <Sider className="layout-slide-cus" theme="dark" trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className={classnames("logo", { "collapsed-logo": this.state.collapsed })}
          // onClick={()=> browserHistory.push("/home")}
          >
            <img src={convertIcon} style={{ height: 40, width: 'auto' }} alt="img" />
            <img src={HusConvert} alt="img" className="hus-convert" />
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={this.handleSelectItem()}>
            <Menu.Item key="1" onClick={() => browserHistory.push("/home")} className="item-menu-cus">
              <Icon type="home" />
              <span>Home</span>
            </Menu.Item>
            <Menu.Item key="2" onClick={() => browserHistory.push("/user")} className="item-menu-cus">
              <Icon type="user" />
              <span>Users</span>
            </Menu.Item>
            <Menu.Item key="3" onClick={() => browserHistory.push("/class")} className="item-menu-cus">
              <Icon type="read" />
              <span>Classes</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ display: 'flex', justifyContent: 'space-between', background: '#fff', padding: 0, paddingRight: 24 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <DropDownCus onClickAccount={() => this.setState({ showMyAccountModal: true })} >
              <Avatar hasName avatar={!_.isEmpty(user) ? getUrlFile(user.user.avatarId) : null} name={!_.isEmpty(user) ? user.user.name : ""} />
            </DropDownCus>
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
            <Route exact path="/home" render={() => !_.isEmpty(user) ? (<Home />) : (<Redirect to="/login" />)} />
            <Route exact path="/user" render={() => !_.isEmpty(user) ? (<User />) : (<Redirect to="/login" />)} />
            <Route exact path="/class" render={() => !_.isEmpty(user) ? (<ClassRoom />) : (<Redirect to="/login" />)} />
          </Content>
        </Layout>

        {!_.isEmpty(user) ?
          <MyAccountModal
            visible={showMyAccountModal}
            onCancel={() => { this.setState({ showMyAccountModal: false }) }}
          />
          : null}
      </Layout>
    );
  }

}
const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps)(AppLayout)