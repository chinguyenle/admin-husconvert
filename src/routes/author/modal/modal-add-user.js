import React, { Component } from 'react'

import { Modal, notification } from 'antd';
import _ from 'lodash'
import classnames from 'classnames'
import './modal-add-user.scss'
import swal from 'sweetalert';
import { apiClass } from '../../../api/api-class';
import ButtonBorder from '../../../components/button/button-border';
import { SelectUser } from './select-user';


export class ModalAddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      userNotInClass:[]
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      console.log('change');
     (nextProps.addUser&&nextProps.classId)&&this.getUserNotInClass(nextProps.classId)
      this.setState({
        visible: nextProps.visible,

      })
    }
  }
  componentDidMount() {
    const { visible, classId, addUser } = this.props;
    addUser&&classId&&this.getUserNotInClass(classId);
    this.setState({
      visible: visible,
    })
  }



  cancel() {
    const { onCancel } = this.props;
    this.setState({
      visible: false,
      userNotInClass:[]
    }, () => onCancel())
  }
  getUserNotInClass(classId) {
    apiClass.getUserNotInClass(classId)
      .then(res => {
        console.log('res',res)
        if (res.size > 0) {
          this.setState({
            userNotInClass: res.content
          })
        }
      })
      .catch(e => console.log(e))
  }

  addUserToClass(listIndex, callBack) {
    const { classId } = this.props;
    swal({
      title: "Add new student",
      icon: "warning",
      buttons: true,
    }).then((value) => {
      if (value) {
        let { userNotInClass } = this.state;
        let user = [];
        listIndex.map(item => {
          let userTemp = _.find(userNotInClass, user => user.id === Number(item));
          if (userTemp)
            user.push(userTemp)
        })
        // console.log("user  add on", user, classId)
        apiClass.addUserToClass(classId, user)
          .then(res => {
            console.log("ress", res);
            this.cancel()
            // this.setState({
            //   classDetail: res.classRoom
            // })
            // this.getUserNotInClass(); 
            callBack()
            swal({
              title: "Success",
              icon: "success",
            })
          })
          .catch(e => console.log('e', e))
      }
    })

  }
  deleteUser(listIndex, callBack) {
    const { classId, usersInClass } = this.props;
    // const { usersInClass } = this.state;
    swal({
      title: "Delete student?",
      icon: "warning",
      buttons: true,
    }).then((value) => {
      if (value) {
        let user = [];
        listIndex.map(item => {
          let userTemp = _.find(usersInClass, user => user.id === Number(item));
          if (userTemp)
            user.push(userTemp)

        })
        apiClass.deleteUser(classId, user)
          .then(res => {
            console.log('res', res)
            if (res.classRoom) {
              swal({
                title: "Students have been deleted",
                icon: "success",
              })
              this.cancel()
              // this.setState({
              //   classDetail: res.classRoom
              // })
              // this.getUserNotInClass(); 
              callBack()
            }
          })

          .catch(e => console.log('e', e))
      }
    })

  }


  render() {
    const { addUser,  usersInClass} = this.props;
    const { visible, userNotInClass } = this.state;
    console.log('modal add', this.props)
    return (
      <Modal
        visible={visible}
        footer={[
          <ButtonBorder content={"Cancel"} onClick={() => this.cancel()} />,
        ]}
        className="modal-add-lesson"
        onCancel={() => this.cancel()}
      >
        <div className="add-lesson-container">
          <div className="add-lesson-content">
            {addUser
              ?
              <SelectUser
                listUsers={userNotInClass}
                onAdd={(listIndex, callBack) => this.addUserToClass(listIndex, callBack)}
              />
              :
              <SelectUser
                title="Delete student"
                buttonTitle="delete"
                listUsers={usersInClass}
                onAdd={(listIndex, callBack) => this.deleteUser(listIndex, callBack)}
              />
            }

          </div>
        </div>
      </Modal>
    );
  }
}
