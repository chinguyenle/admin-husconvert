import React, { Component } from 'react'

import {Modal, Form, Input, DatePicker, Icon, Button, Avatar, Upload, notification } from 'antd';
import _ from 'lodash'
import classnames from 'classnames'
import './modal-add-user.scss'
import './modal-add-account.scss'
import swal from 'sweetalert';
import { apiClass } from '../../../api/api-class';
import ButtonBorder from '../../../components/button/button-border';
import { SelectUser } from './select-user';
import { formateDate } from '../../../util/util';
import { apiAuth } from '../../../api/api-auth';


class ModalAddAccountForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      confirmDirty: false,
      autoCompleteResult: [],
      avatar: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      // (nextProps.addUser && nextProps.classId) && this.getUserNotInClass(nextProps.classId)
      this.setState({
        visible: nextProps.visible,

      })
    }
  }
  componentDidMount() {
    const { visible, classId, addUser } = this.props;
    // addUser && classId && this.getUserNotInClass(classId);
    this.setState({
      visible: visible,
    })
  }



  cancel() {
    const { onCancel } = this.props;
    this.setState({
      visible: false,
      // userNotInClass: []
    }, () => onCancel())
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let params = Object.assign(values, { avatar: this.state.avatar });
        params.dateOfBirth = formateDate(values.dateOfBirth);
        apiAuth.signup(params)
          .then(res => {
            console.log('res', res)
            if (res.success == true) {
              swal({
                title: "Your account is created!",
                text: "Please, login to continue",
                icon: "success",
              }).then((value) => {
                this.cancel()
              })

            } else {
              swal({
                title: 'Creating account is failed',
                text: res.message || 'There was some error during account creation, Please try again!',
                icon: "error",
              })
            }
          })
          .catch(e => {
            swal({
              title: 'Creating account is failed',
              text: e || 'There was some error during account creation, Please try again!',
              icon: "error",
            })
          })
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }


  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, userNotInClass } = this.state;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
      labelAlign: "left"
    };
    return (
      <Modal
        visible={visible}
        footer={[
          <ButtonBorder content={"Cancel"} onClick={() => this.cancel()} />,
        ]}
        className="modal-add-lesson modal-add-account"
        onCancel={() => this.cancel()}
      >
        <Form onSubmit={this.handleSubmit} className="register-form" layout="horizontal"
          {...formItemLayout}
          encType="multipart/form-data"
        >
          <Form.Item
            label="username"
          >
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item
            label="Email"
          >
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: 'The input is not valid E-mail!', },
                { required: true, message: 'Please input your email!' }],
            })(
              <Input placeholder="Email" />
            )}
          </Form.Item>
          <Form.Item
            label="name"
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your name!' }],
            })(
              <Input placeholder="name" />
            )}
          </Form.Item>
          <Form.Item
            label="Date of birth"
          >
            {getFieldDecorator('dateOfBirth', {
              rules: [{ required: true, message: 'Please input your date of birth!' }],
            })(

              <DatePicker />
            )}
          </Form.Item>
          <Form.Item
            label="Password"
          >
            {getFieldDecorator('password', {
              rules: [{
                required: true, message: 'Please input your password!',
              }, {
                validator: this.validateToNextPassword,
              }],
            })(
              <Input type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item
            label="Confirm password"
          >
            {getFieldDecorator('confirm-password', {
              rules: [{
                required: true, message: 'Please confirm your password!',
              }, {
                validator: this.compareToFirstPassword,
              }],
            })(
              <Input type="password" placeholder="Confirm Password" onBlur={this.handleConfirmBlur} />
            )}
          </Form.Item>
          <ButtonBorder
            content={"Register"}
            type="submit"
          />
        </Form>
      </Modal>
    );
  }
}
export const ModalAddAccount = Form.create({ name: 'ModalAddAccountForm' })(ModalAddAccountForm);