import React, { Component } from 'react'

import './home.scss'
import { Button } from 'antd';
import {  Bar } from 'react-chartjs';
import { apiUser } from '../../api/api-user';
import { apiClass } from '../../api/api-class';
import CanvasJSReact from '../../components/canvasjs-2.3.1/canvasjs.react';
import { ROLE } from '../../constants';


const CanvasJS = CanvasJSReact.CanvasJS;
const CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      classRoom: []
    }
  }
  componentDidMount() {
    this.getAllUser();
    this.getAllClasses();
  }

  getAllUser() {
    apiUser.getAllUser()
      .then(res => {
        if (res.size > 0) {
          this.setState({
            users: res.content
          })
        }
      })
      .catch(e => console.log(e))
  }
  getAllClasses = () => {
    apiClass.getAllClasses()
      .then(res => {
        if (res.size > 0) {
          this.setState({
            classRoom: res.content
          })
        }
      })
      .catch(e => console.log(e))
  }
  drawBarChar() {
    const {classRoom} = this.state;
    let classRoomNames = classRoom.map(item => item.classRoom.classRoomName)
    console.log('nats', classRoomNames)
    let usersInClass = classRoom.map(item => item.classRoom.users.length)
    let lessonsInClass = classRoom.map(item => item.classRoom.lessons.length)
    console.log('nats', classRoomNames, usersInClass, lessonsInClass)
    let data = {
      labels: classRoomNames,
      datasets: [
        {
          label: "Users",
          fillColor: "rgba(220,220,220,0.5)",
			strokeColor: "rgba(220,220,220,0.8)",
			highlightFill: "rgba(220,220,220,0.75)",
			highlightStroke: "rgba(220,220,220,1)",
          data: usersInClass
        },
        {
          label: "lessons",
          fillColor: "rgba(151,187,2,0.5)",
          strokeColor: "rgba(151,187,2,0.8)",
          highlightFill: "rgba(151,187,2,0.75)",
          highlightStroke: "rgba(151,187,205,1)",
          data: lessonsInClass
        }
      ]
    }

    return (
      <div className="pie-chart-cus-container">
        <h1>Class chart</h1>
        <Bar data={data} width="600" height="250" />
        
      </div>
    )
  }
  render() {
    const { users, classRoom } = this.state;
    const allUser = users.length;
    let student = 0;
    let teacher = 0;
    if (users.length > 0) {

      users.forEach(users => {
        if (users.roles[0].name === ROLE.ROLE_TEACHER)
          teacher++;
        else student++;
      })
    }
    const options = {
      animationEnabled: true,
      exportEnabled: true,
      theme: "dark2", // "light1", "dark1", "dark2"
      title: {
        text: "User Type"
      },
      data: [{
        type: "pie",
        indexLabel: "{y}%",
        indexLabelPlacement: "inside",
        startAngle: -90,
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        dataPoints: [
          { y: student * 100 / allUser, label: "Student" },
          { y: teacher * 100 / allUser, label: "Teacher" },
        ]
      }]
    }

    console.log('staet', this.state)
    return (
      <div className="home">
        <div className="user-chart">

          <div className="pie-chart-cus-container">
            <h1>User chart</h1>
            <CanvasJSChart options={options} />
          </div>
          <div className="count-user"><span>All: {allUser}</span><span>Student: {student}</span><span>Teacher: {teacher}</span></div>

        </div>
        <div className="user-chart class-chart">
          {this.state.classRoom.length >0 &&this.drawBarChar()}
          <div className="count-user"><span>All: {this.state.classRoom.length} classes</span></div>
        </div>
      </div>
    );
  }
}