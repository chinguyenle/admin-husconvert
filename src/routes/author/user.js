import React, { Component } from 'react'
import swal from 'sweetalert';
import { Table, Input, Button, Icon, Tag, InputNumber, Popconfirm, Form, DatePicker, Radio } from 'antd';
import Highlighter from 'react-highlight-words';
import moment from 'moment'
import './user.scss'
import { apiUser } from '../../api/api-user';
import { ROLE } from '../../constants';
import { formateDate } from '../../util/util';
import { ModalAddAccount } from './modal/modal-add-account';

const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    console.log('prosss', this.props);
    if (this.props.inputType() === 'number') {
      return <InputNumber />;
    }
    if (this.props.inputType() === 'date') {
      return <DatePicker />;
    } if (this.props.inputType() === 'roles') {
      return (
        <Radio.Group >
        <Radio value={"ROLE_USER"}>Student</Radio>
        <Radio value={"ROLE_TEACHER"}>Teacher</Radio>
        
        </Radio.Group>);
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: inputType() === "email"
                ? [
                  {
                    required: true,
                    message: `Please Input ${title}!`,
                  },

                  { type: 'email', message: 'The input is not valid E-mail!' }
                ]
                : [

                  {
                    required: true,
                    message: `Please Input ${title}!`,
                  }
                ],
              initialValue: inputType() === 'date' ? moment(record[dataIndex]) :inputType() === 'roles'?record.roles[0].name :record[dataIndex],
            })(this.getInput())}
          </Form.Item>
        ) : (
            children
          )}
      </td>
    );
  };

  render() {
    // console.log('o ptops', this.props)
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}
class UserTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editingKey: '',
      searchText: '',
      users: null,
      showModalAddAccount: false
    }
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        width: '5%',
        editable: false,
      },
      {
        title: 'UserName',
        dataIndex: 'username',
        key: 'username',
        width: '20%',
        ...this.getColumnSearchProps('username'),
        editable: false,
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '20%',
        ...this.getColumnSearchProps('name'),
        editable: true,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        width: '15%',
        editable: true,
      },
      {
        title: 'Date Of Birth',
        dataIndex: 'dateOfBirth',
        render: (text, record, index) => (<DatePicker defaultValue={moment(text)} disabled/>),
        key: 'date',
        width: '15%',
        editable: true,
      },
      {
        title: 'Role',
        dataIndex: 'roles',
        key: 'role',
        width: '8%',
        editable: true,
        render: roles => (
          <span>
            {roles.map(role => {
              let color = role.name.length > 9 ? 'geekblue' : 'green';
              if (role === 'loser') {
                color = 'volcano';
              }
              return (
                role.name === ROLE.ROLE_USER ?
                  <Tag color={color} key={role.id}>
                    {"Student"}
                  </Tag>
                  :
                  <Tag color={color} key={role.id}>
                    {"Teacher"}
                  </Tag>
              );
            })}
          </span>
        ),
        filters: [{ text: "Student", value: ROLE.ROLE_USER }, { text: 'Teacher', value: ROLE.ROLE_TEACHER }],
        onFilter: (value, record) => record.roles[0].name.indexOf(value) === 0,
        editable: true,
      },
      {
        title: 'Action',
        dataIndex: 'operation',
        width: '15%',
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    href="javascript:;"
                    onClick={() => this.save(form, record.id)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.id)}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <span>
                <a disabled={editingKey !== ''} onClick={() => this.edit(record.id)} style={{}}>
                  Edit
              </a>
                <a disabled={editingKey !== ''} onClick={() => this.delete(record.id)} style={{marginRight:20,marginLeft:20, color:'red'}}>
                  Delete
              </a>
                <a disabled={editingKey !== ''} onClick={() => this.resetPassword(record.id)} style={{color:'red'}}>
                  Reset
              </a>
            </span>
            );
        },
      }
    ];

  }

  //
  isEditing = record => record.id === this.state.editingKey;
  cancel = () => {
    this.setState({ editingKey: '' });
  };

  save(form, key) {
    form.validateFields((error, row) => {
      console.log('error', error, row)
      if (error) {
        return;
      }
      
      const newData = [...this.state.users];
      const index = newData.findIndex(item => key === item.id);
      console.log('index',key, index, row)
      if (index > -1) {
        swal({
          title: "Save your change ?",
          icon: "warning",
          buttons: true,
        })
        .then((value) => {
          if (value) {
            let params ={
              ...row,
              role:row.roles,
              dateOfBirth: formateDate(row.dateOfBirth),
              newPassword:"",
              avatar:null,
              userId: key
            }
            apiUser.editUser(params)
            .then(res => {
              console.log('res', res)
              if(res.id){
                swal({
                  title: "Information is changed",
                  icon: "success",
                  buttons: true,
                })
                this.getAllUser();
                this.setState({ editingKey: '' });
              }
            })
          } 
        });
        
      } else {
        newData.push(row);
        this.setState({ users: newData, editingKey: '' });
      }
    });
  }

  edit(key) {
    this.setState({ editingKey: key });
  }

  delete(key) {
    swal({
      title: "Delete this user",
      text: "when user is deleted, all user's infomation is delete too!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    })
    .then((willDelete) => {
      if (willDelete) {
        apiUser.deleteUser(key)
        .then(res => {console.log('res', res);
          if(res.success){
            swal({
              title: "User is deleted",
              icon: "success",
              buttons: true,
            })
            this.getAllUser();
          }
            
      })
        .catch(e => console.log(e))
        
      }
    });
  }

  resetPassword(key){
    swal({
      title: "Reset passoword",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true
    })
    .then((willDelete) => {
      if (willDelete) {
        apiUser.resetPassword(key)
        .then(res => {console.log('res', res);
        if(res.success){
            swal({
              title: "Information is change",
              icon: "success",
              buttons: true,
            })
            this.getAllUser();
          }
      })
        .catch(e => console.log(e))
        
      }
    });
  }

  //
  componentDidMount() {
    this.getAllUser();
  }

  getAllUser() {
    apiUser.getAllUser()
      .then(res => {
        console.log('res', res);
        if (res.size > 0) {
          this.setState({
            users: res.content
          })
        }
      })
      .catch(e => console.log(e))
  }


  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
      </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
      </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      text ? <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
        : null
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };
  handleTableChange = (pagination, filters, sorter) => {

  }

  openModalAddUser(){
    this.setState({
      showModalAddAccount: true
    })
  }
  render() {
    const { users, showModalAddAccount } = this.state;
    const components = {
      body: {
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: () => {
            if (col.dataIndex === 'dateOfBirth') return 'date';
            if (col.dataIndex === 'email') return 'email';
            if (col.dataIndex === 'roles') return 'roles';
            return "text"
          },
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });
    return (
      // users ?
      <EditableContext.Provider value={this.props.form}>
         <Button onClick={()=> this.openModalAddUser()} type="primary" style={{ marginBottom: 16 }}>
          Add user
        </Button>
        <Table
          components={components}
          rowKey={record => record.id}
          columns={columns}
          dataSource={users}
          pagination={{
            onChange: this.cancel,
          }}
          rowClassName="editable-row"
          bordered
        />
          <ModalAddAccount
            visible={showModalAddAccount}
            onCancel={()=>{this.setState({showModalAddAccount: false}); this.getAllUser()}}
      />
      </EditableContext.Provider>
      // : null
    );

  }
}
const User = Form.create()(UserTable);

export default User