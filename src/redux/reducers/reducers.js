import { combineReducers } from "redux";

import {lesson} from './lesson';
import {user} from './user'

export const reducers = (state, action) => combineReducers({
    lesson, user
})(state, action)