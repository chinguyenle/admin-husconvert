

export const lesson=(state={}, action)=>{
    switch (action.type){
        
        case 'UPDATE_LESSON':
            return{
                ...state,
                ...action.slide
            };
        case 'DELETE_LESSON':
            return {};

        default: return state;
    }
}